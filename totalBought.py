allGuests = {'Imane': {'apples':4, 'sandwiches':10, 'cups':5},
             'Marouane': {'chairs':2, 'oranges': 8},
             'Carol': {'apples':5, 'oranges': 3}}

def totalBought(guests, item):
    numBought = 0
    for k, v in guests.items():
        numBought = numBought + v.get(item, 0)
    return numBought

print ('Number of things being bought:')
print ('- Apples    ' + str(totalBought(allGuests, 'apples')))
print ('- Sandwiches    ' + str(totalBought(allGuests, 'sandwiches')))
print ('- Oranges    ' + str(totalBought(allGuests, 'oranges')))
print ('- Cups    ' + str(totalBought(allGuests, 'cups')))

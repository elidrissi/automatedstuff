#justifying text rjust(), ljust(), center()

def printPicnic(itemsDict, leftWidth, rightWidth):
    print ('PICNIC ITEMS'.center(leftWidth+rightWidth, '-'))
    for k, v in itemsDict.items():
        print(k.ljust(leftWidth, '.') + str(v).rjust(rightWidth))

picnicItems = {'sandwiches':5, 'apples':10, 'oranges':3, 'cups':5}
printPicnic(picnicItems, 12, 4)
printPicnic(picnicItems, 20, 5)

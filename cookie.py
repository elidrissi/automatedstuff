import random

messages = ['Remember, you are beautiful',
            'No one will do the job for you, no one, except you',
            "Luck doesn't exist, hardwork takes you directly to the goal",
            "Help other human being, help humanity",
            "There's no shame in crying, cry when you feel like so"
            ]

print (messages[random.randint(0, len(messages) -1)])
